## Penggunaan API

Untuk menggunakan API ini jalankan NodeJs nya dengan menggunakan url misalnya:

```bash
localhost:4000
link ini kita beri nama {url}
```

### Register
```bash
[POST] => {url}/auth/register
header {
   "Content-Type": "application/json"
}

body {
   "name": "Mirza Purnandi",
   "username": "mirza",
   "password": "password"
}
```
### Login
```bash
[POST] => {url}/auth/login
header {
   "Content-Type": "application/json"
}

body {
   "username": "mirza",
   "password": "password"
}
```
### Get Parameter Page
misalnya page=1
```bash
[GET] => {url}/job/p?page=1
header {
   "Content-Type": "application/json"
}
```
### Get Parameter Description / location
misalnya description=python&location=berlin
```bash
[GET] => {url}/job/p?description=python&location=berlin
header {
   "Content-Type": "application/json"
}
```
## Contributing
Create By Mirza Purnandi

## License
[MIT](https://choosealicense.com/licenses/mit/)