const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
//const bodyParser = require('body-parser')
const dbConfig = require('./database/db')
const api = require('./routes/auth.routes')
const job = require('./routes/job')

//Express
const app = express();
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(cors());

app.use('/public', express.static('public'));

//Route
app.use('/auth', api)
app.use('/job', job)

// Define PORT
const port = process.env.PORT || 4000;
dbConfig().then(_ => {
  app.listen(port, _ => {
    console.log(`Server started on port ${port}`)
  })
})

// Express error handling
app.use((req, res, next) => {
    setImmediate(() => {
        next(new Error('Sepertinya ada yang Salah!!!'));
    });
});

app.use(function (err, req, res, next) {
    console.error(err.message);
    if (!err.statusCode) err.statusCode = 500;
    res.status(err.statusCode).send(err.message);
});