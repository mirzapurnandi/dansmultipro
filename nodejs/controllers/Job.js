const axios = require('axios')
const url = 'http://dev3.dansmultipro.co.id/api/recruitment/positions.json';

const getAll = async (req, res, next) => {
    try {
        let data = await axios.get(url);
        let showData = await data.data;

        res.status(201).json({
        success: true,
        result: showData
    });
    } catch (error) {
        return res.status(422).json({
            success: false,
            errors: error
        })
    }
}

const getParameter = async (req, res, next) => {
    let urlParameter = url;
    let query = req.query;
    let show = '';

    if(query.page){
        urlParameter += `?page=${query.page}`
    }
    if(query.description){
        if(query.page){
            show = '&'
        } else {
            show = '?'
        }
        urlParameter += `${show}description=${query.description}`
    }
    if(query.location){
        if(query.location) show = '&'
        urlParameter += `${show}location=${query.location}`
    }

    try {
        let data = await axios.get(urlParameter);
        let showData = await data.data;

        res.status(201).json({
            success: true,
            result: showData
        });
    } catch (error) {
        return res.status(422).json({
            success: false,
            errors: error
        })
    }
}

const getById = async (req, res, next) => {
    let id = await req.params.id;
    try {
        let data = await axios.get(`http://dev3.dansmultipro.co.id/api/recruitment/positions/${id}`);
        let showData = await data.data;

        res.status(201).json({
            success: true,
            result: showData
        });
    } catch (error) {
        return res.status(422).json({
            success: false,
            errors: error
        })
    }
}

module.exports = { getAll, getParameter, getById }