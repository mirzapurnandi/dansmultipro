const userSchema = require('../models/User')
const bcrypt = require('bcrypt')
const { validationResult } = require('express-validator')
const CustomError = require('../models/CustomError')
const jwt = require('jsonwebtoken')

const signUp = async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({
            success: false,
            errors: errors.array()
        })
    } else {
        const { name, username, password } = req.body

        let user = await userSchema.findOne({ username })
        if (user) {
            return next(
                new CustomError('Username already exists', 403)
            )
        }
        const salt = await bcrypt.genSalt(10)
        const hashedPassword = await bcrypt.hash(password, salt)
        user = new userSchema({
            name: name,
            username: username,
            password: hashedPassword
        });
        await user.save()

        res.status(201).json({
            success: true,
            message: "User successfully created!",
            result: user
        });
    }
}

const login = async (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({
            success: false,
            errors: errors.array()
        })
    } 

    const { username, password } = req.body

    try {
        let user = await userSchema.findOne({username})
        if (!user) return next(new CustomError('Invalid credentials 1', 400))

        const isMatch = await bcrypt.compare(password, user.password)
        if(!isMatch){
            return next(new CustomError('Invalid credentials 2', 400))
        }

        let jwtToken = jwt.sign({
            username: user.username,
            userId: user._id
        }, "longer-secret-purnandi", {
            expiresIn: "1h"
        });
        res.status(200).json({
            token: jwtToken,
            expiresIn: 3600,
            msg: user
        });
    } catch (error) {
        console.log(error);
        next(new CustomError('Something went wrong', 500))
    }
}

const getAll = async (req, res) => {
    userSchema.find((error, response) => {
        if (error) {
            return next(error)
        } else {
            res.status(200).json({
                success: true,
                result: response
            })
        }
    })
}

module.exports = { signUp, login, getAll }