const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//const uniqueValidator = require('mongoose-unique-validator');

const userSchema = new Schema({
    name: {
        type: String
    },
    username: {
        type: String,
        unique: true
    },
    password: {
        type: String
    }
}, {
    collection: 'users'
})

//userSchema.plugin(uniqueValidator, { message: 'Username already in use.' });
module.exports = mongoose.model('User', userSchema)