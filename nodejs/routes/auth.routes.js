const express = require("express")
const router = express.Router()
const { check } = require('express-validator')
const userController = require('../controllers/User')
const authorize = require('../middlewares/auth')

// Sign-up
router.post("/register", [
        check('name').not().isEmpty().withMessage('Nama tidak boleh kosong'),
        check('username').not().isEmpty().isLength({min:3}).withMessage('Username minimal 3 karakter'),
        check('password').not().isEmpty().isLength({min:5, max:10}).withMessage('Password Minimal 5 dan Maksimal 10 karakter'),
    ],
    userController.signUp
);

// Login
router.post("/login", [
        check('username', 'Masukan username Anda').trim().not().isEmpty(),
        check('password', 'Masukan password yang Valid').trim().isLength({min: 5})
    ], 
    userController.login
);

router.get("/user", authorize, userController.getAll)
module.exports = router;