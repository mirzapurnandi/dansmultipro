const express = require("express")
const router = express.Router()
const authorize = require('../middlewares/auth')
const jobController = require('../controllers/Job')

router.get("/", authorize, jobController.getAll)
router.get("/p", authorize, jobController.getParameter)
router.get("/positions/:id", authorize, jobController.getById)
module.exports = router;